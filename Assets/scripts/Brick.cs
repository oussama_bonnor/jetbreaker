﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    public int maxHits;
    public int timesHits;


    // Use this for initialization
    void Start()
    {
        timesHits = 0;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "ball")
        {
            ++timesHits;
            SpriteRenderer sprite = GetComponent<SpriteRenderer>();
            //decreasing the alpha of the block to tell user it s taking dammage
            float alpha = sprite.color.a / maxHits;
            sprite.color = new Color(sprite.color.r,sprite.color.g,sprite.color.b,sprite.color.a - alpha); 
            if (timesHits >= maxHits)
            {

                Destroy(gameObject);
            }
        }
    }
}