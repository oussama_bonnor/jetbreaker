﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BrickBehavior : MonoBehaviour
{
    
    private GameObject yellowBall;

    private Rigidbody2D myRigidbody;
    bool gameStarted;
    float maxWidth;

    Vector3 screenWidth;

    Vector3 edgeOfScreen;

    // Use this for initialization
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        gameStarted = false;
        screenWidth = new Vector3(Screen.width, 0, 0);
        edgeOfScreen = Camera.main.ScreenToWorldPoint(screenWidth);
        maxWidth = edgeOfScreen.x - transform.localScale.y / 2;
        yellowBall = GameObject.Find("yellow ball");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 v2 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float xPosition = Mathf.Clamp(v2.x, -maxWidth, maxWidth);
        myRigidbody.transform.position = new Vector3(xPosition, transform.position.y, 0f);
        if (yellowBall.transform.position.y < transform.position.y)
        {
            StartCoroutine(loseFunction());
        }
    }

    private void FixedUpdate()
    {
        if (!gameStarted && Input.GetMouseButtonDown(0))
        {
            gameStarted = true;
            yellowBall.transform.parent = null;
            yellowBall.GetComponent<Rigidbody2D>().velocity = new Vector2(1f, 10f);
            yellowBall.GetComponentInChildren<ParticleSystem>().Play();
        }
    }

    IEnumerator loseFunction()
    {
      yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("Stuff");
    }


}